(function() {
  'use strict';

  function fileInput($parse) {
    return {
      restrict : 'A',
      link: function(scope, elem, attrs){
        elem.bind('change', function(){
          $parse(attrs.fileInput).assign(scope, elem[0].files);
          scope.$apply();
        });
      }
    };
  }

  angular.module('common.directives.fileinput', [])
    .directive('fileInput', fileInput);
})();
