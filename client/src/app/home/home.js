(function() {
  'use strict';

  /**
   * @name  config
   * @description config block
   */
  function config($stateProvider) {
    $stateProvider
      .state('root.home', {
        url: '/',
        views: {
          '@': {
            templateUrl: 'src/app/home/home.tpl.html',
            controller: 'HomeCtrl as home'
          }
        }
      });
  }

  /**
   * @name  HomeCtrl
   * @description Controller
   */
  function HomeCtrl($scope, UserService, $rootScope) {
      $scope.vm = this;

      $scope.vm.user = null;
      $scope.vm.allUsers = [];
      $scope.vm.deleteUser = deleteUser;

      initController();

      function initController() {
          loadCurrentUser();
          loadAllUsers();
      }

      function loadCurrentUser() {
          UserService.GetByUsername($rootScope.globals.currentUser.username)
              .then(function (user) {
                  $scope.vm.user = user;
              });
      }

      function loadAllUsers() {
          UserService.GetAll()
              .then(function (users) {
                  $scope.vm.allUsers = users;
              });
      }

      function deleteUser(id) {
          UserService.Delete(id)
          .then(function () {
              loadAllUsers();
          });
      }

  }

  angular.module('home', [])
    .config(config)
    .controller('HomeCtrl', HomeCtrl);
})();
