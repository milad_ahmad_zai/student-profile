﻿(function() {
  'use strict';

  /**
   * @name  config
   * @description config block
   */
  function config($stateProvider) {
    $stateProvider
      .state('root.register', {
        url: '/register',
        views: {
          '@': {
            templateUrl: 'src/app/register/register.tpl.html',
            controller: 'RegisterController'
          }
        }
      });
  }

  /**
   * @name  RegisterController
   * @description Controller
   */
    RegisterController.$inject = ['$scope', 'UserService', '$location', '$rootScope', 'FlashService'];
    function RegisterController($scope, UserService, $location, $rootScope, FlashService) {
        $scope.vm = this;

        $scope.vm.register = register;

        function register() {
            $scope.vm.dataLoading = true;
            UserService.Create($scope.vm.user)
                .then(function (response) {
                    if (response.success) {
                        FlashService.Success('Registration successful', true);
                        $location.path('/login');
                    } else {
                        FlashService.Error(response.message);
                        $scope.vm.dataLoading = false;
                    }
                });
        }
    }

  angular.module('register', [])
    .config(config)
    .controller('RegisterController', RegisterController);
})();